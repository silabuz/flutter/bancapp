# Bancapp

## :+1: Sígueme en mis redes sociales:

- WebPersonal: (https://jorge-vicuna.gitlab.io/jorge-vicuna/)
- GitLab: (https://gitlab.com/jorge_vicuna)
- Youtube: (https://www.youtube.com/channel/UCW0m1TKKiN3Etejqx6h3Jtg)
- Linkedin: (https://www.linkedin.com/in/jorge-vicuna-valle/)
- Facebook: (https://www.facebook.com/jorge.vicunavalle/)

<table>
    <td align="center" >
      <a href="https://jorge-vicuna.gitlab.io/jorge-vicuna/">
        <img src="https://jorge-vicuna.gitlab.io/jorge-vicuna/static/media/avatar.272f0e79.jpg" width="100px;" alt=""/>
        <br />
        <sub><b>Jorge Vicuña Valle</b></sub>
      </a>
            <br />
      <span>♌🍗🎸🏀</span>
    </td>
</Table>
