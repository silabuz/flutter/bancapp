import 'package:flutter/material.dart';

import 'package:bancapp/models/models.dart';
import 'package:bancapp/screens/screens.dart';

class AppRoutes {
  static const initialRoute = 'login';

  static final menuOptions = <MenuOption>[
    // MenuOption(route: 'home', name: 'Home Screen', screen: const HomeScreen(), icon: Icons.home_max_sharp ),
    MenuOption(
        route: 'perfil',
        name: 'Perfil',
        screen: const Perfilcreen(),
        icon: Icons.list_alt),
    MenuOption(
        route: 'asistencia',
        name: 'Asistencia',
        screen: const AsistenciaScreen(),
        icon: Icons.list),
    MenuOption(
        route: 'adelanto',
        name: 'Adelanto',
        screen: const AdelantoScreen(),
        icon: Icons.credit_card),
    MenuOption(
        route: 'credito',
        name: 'Credito',
        screen: const CreditoScreen(),
        icon: Icons.add_alert_outlined),
    MenuOption(
        route: 'menu',
        name: 'Menu',
        screen: const MenuScreen(),
        icon: Icons.supervised_user_circle_outlined),
    MenuOption(
        route: 'recover',
        name: 'recover',
        screen: const RecoverScreen(),
        icon: Icons.supervised_user_circle_outlined),
  ];

  static Map<String, Widget Function(BuildContext)> getAppRoutes() {
    Map<String, Widget Function(BuildContext)> appRoutes = {};
    appRoutes.addAll({'login': (BuildContext context) => const LoginScreen()});

    for (final option in menuOptions) {
      appRoutes.addAll({option.route: (BuildContext context) => option.screen});
    }

    return appRoutes;
  }

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) => const HomeScreen(),
    );
  }
}
