import 'package:bancapp/utils/extensions.dart';
import 'package:flutter/material.dart';

class InputDecorations {
  static InputDecoration authInputDecoration({
    required String hintText,
    required String labelText,
    IconData? prefixIcon,
    IconData? suffixIcon,
  }) {
    return InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: '#004650'.toColor()),
        ),
        focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: '#004650'.toColor(), width: 2)),
        hintText: hintText,
        labelText: labelText,
        labelStyle: const TextStyle(color: Colors.grey),
        prefixIcon: prefixIcon != null
            ? Icon(prefixIcon, color: '#004650'.toColor())
            : null);
  }
}
