import 'dart:convert';
import 'package:bancapp/models/models.dart';
import 'package:bancapp/config.dart';
import 'package:bancapp/services/shared_service.dart';
import 'package:http/http.dart' as http;

class APIService {
  static var client = http.Client();

  static Future<bool> login(UserRequestModel model) async {
    Map<String, String> requestHeaders = {
      'Content-Type': 'application/json',
    };

    var url = Uri.http(Config.apiURL, Config.loginAPI);

    var response = await client.post(
      url,
      headers: requestHeaders,
      body: jsonEncode(model.toJson()),
    );

    if (response.statusCode == 200) {
      //SHARED
      await SharedService.setLoginDetails(
          userResponseModelFromJson(response.body));
      return true;
    } else {
      return false;
    }
  }
}
