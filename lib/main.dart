import 'package:bancapp/services/services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bancapp/theme/app_theme.dart';
import 'package:bancapp/router/app_routes.dart';

import 'package:bancapp/providers/ui_provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UiProvider()),
      ],
      child: MaterialApp(
          scaffoldMessengerKey: NotificationsService.messengerKey,
          debugShowCheckedModeBanner: false,
          title: 'Material App',
          initialRoute: AppRoutes.initialRoute,
          routes: AppRoutes.getAppRoutes(),
          onGenerateRoute: AppRoutes.onGenerateRoute,
          theme: AppTheme.lightTheme),
    );
  }
}
