import 'dart:convert';

UserRequestModel userRequestModelFromJson(String str) =>
    UserRequestModel.fromJson(json.decode(str));

String userRequestModelToJson(UserRequestModel data) =>
    json.encode(data.toJson());

class UserRequestModel {
  UserRequestModel({
    required this.email,
    required this.password,
  });

  String email;
  String password;

  factory UserRequestModel.fromJson(Map<String, dynamic> json) =>
      UserRequestModel(
        email: json["email"],
        password: json["password"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
      };
}
