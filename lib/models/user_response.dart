import 'dart:convert';

UserResponseModel userResponseModelFromJson(String str) =>
    UserResponseModel.fromJson(json.decode(str));

String userResponseModelToJson(UserResponseModel data) =>
    json.encode(data.toJson());

class UserResponseModel {
  UserResponseModel({
    this.status,
    this.usuario,
    this.token,
  });

  int? status;
  Usuario? usuario;
  String? token;

  factory UserResponseModel.fromJson(Map<String, dynamic> json) =>
      UserResponseModel(
        status: json["status"],
        usuario: Usuario.fromJson(json["usuario"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "usuario": usuario?.toJson(),
        "token": token,
      };
}

class Usuario {
  Usuario({
    this.names,
    this.lastNames,
    this.career,
    this.university,
    this.actualWork,
    this.nameCompany,
    this.linkCv,
    this.address,
    this.phone,
    this.description,
    this.redLinkedin,
    this.redGitLab,
    this.redGitHub,
    this.redYoutube,
    this.redInsta,
    this.redFace,
    this.tecnologyOne,
    this.tecnologyTwo,
    this.tecnologyThree,
    this.email,
    this.img,
    this.activeMember,
    this.rol,
    this.state,
    this.google,
    this.activeInstructor,
    this.uid,
  });

  String? names;
  String? lastNames;
  String? career;
  String? university;
  String? actualWork;
  String? nameCompany;
  String? linkCv;
  String? address;
  String? phone;
  String? description;
  String? redLinkedin;
  String? redGitLab;
  String? redGitHub;
  String? redYoutube;
  String? redInsta;
  String? redFace;
  String? tecnologyOne;
  String? tecnologyTwo;
  String? tecnologyThree;
  String? email;
  String? img;
  bool? activeMember;
  String? rol;
  bool? state;
  bool? google;
  bool? activeInstructor;
  String? uid;

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        names: json["names"],
        lastNames: json["lastNames"],
        career: json["career"],
        university: json["university"],
        actualWork: json["actualWork"],
        nameCompany: json["nameCompany"],
        linkCv: json["linkCV"],
        address: json["address"],
        phone: json["phone"],
        description: json["description"],
        redLinkedin: json["redLinkedin"],
        redGitLab: json["redGitLab"],
        redGitHub: json["redGitHub"],
        redYoutube: json["redYoutube"],
        redInsta: json["redInsta"],
        redFace: json["redFace"],
        tecnologyOne: json["TecnologyOne"],
        tecnologyTwo: json["TecnologyTwo"],
        tecnologyThree: json["TecnologyThree"],
        email: json["email"],
        img: json["img"],
        activeMember: json["activeMember"],
        rol: json["rol"],
        state: json["state"],
        google: json["google"],
        activeInstructor: json["activeInstructor"],
        uid: json["uid"],
      );

  Map<String, dynamic> toJson() => {
        "names": names,
        "lastNames": lastNames,
        "career": career,
        "university": university,
        "actualWork": actualWork,
        "nameCompany": nameCompany,
        "linkCV": linkCv,
        "address": address,
        "phone": phone,
        "description": description,
        "redLinkedin": redLinkedin,
        "redGitLab": redGitLab,
        "redGitHub": redGitHub,
        "redYoutube": redYoutube,
        "redInsta": redInsta,
        "redFace": redFace,
        "TecnologyOne": tecnologyOne,
        "TecnologyTwo": tecnologyTwo,
        "TecnologyThree": tecnologyThree,
        "email": email,
        "img": img,
        "activeMember": activeMember,
        "rol": rol,
        "state": state,
        "google": google,
        "activeInstructor": activeInstructor,
        "uid": uid,
      };
}
