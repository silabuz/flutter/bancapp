import 'package:bancapp/models/models.dart';
import 'package:bancapp/services/services.dart';
import 'package:bancapp/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:bancapp/providers/login_form_provider.dart';
import 'package:provider/provider.dart';

import 'package:bancapp/ui/input_decorations.dart';
import 'package:bancapp/widgets/widgets.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: AuthBackground(
            child: SingleChildScrollView(
      child: Column(
        children: [
          const SizedBox(height: 200),
          CardContainer(
              child: Column(
            children: [
              const SizedBox(height: 10),
              Text('Bienvenid@', style: Theme.of(context).textTheme.headline4),
              const SizedBox(height: 30),
              ChangeNotifierProvider(
                  create: (_) => LoginFormProvider(), child: _LoginForm())
            ],
          )),
          const SizedBox(height: 50),
        ],
      ),
    )));
  }
}

class _LoginForm extends StatefulWidget {
  @override
  State<_LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<_LoginForm> {
  bool _sliderEnabled = true;

  @override
  Widget build(BuildContext context) {
    final loginForm = Provider.of<LoginFormProvider>(context);

    return Form(
      key: loginForm.formKey,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        children: [
          DropdownButtonFormField<String>(
              decoration: const InputDecoration(labelText: 'Empresa'),
              value: 'Silabuz',
              items: const [
                DropdownMenuItem(value: 'Silabuz', child: Text('Silabuz')),
                DropdownMenuItem(value: 'Curso', child: Text('Curso')),
                DropdownMenuItem(
                    value: 'Dynamicall', child: Text('Dynamicall')),
                DropdownMenuItem(value: 'Peru', child: Text('Peru')),
              ],
              onChanged: (value) => {}),
          const SizedBox(height: 30),
          TextFormField(
            autocorrect: false,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecorations.authInputDecoration(
                hintText: 'example@silabuz.com',
                labelText: 'Correo electrónico',
                prefixIcon: Icons.alternate_email_rounded),
            onChanged: (value) => loginForm.email = value,
            validator: (value) {
              String pattern =
                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
              RegExp regExp = RegExp(pattern);

              return regExp.hasMatch(value ?? '')
                  ? null
                  : 'El valor ingresado no luce como un correo';
            },
          ),
          const SizedBox(height: 30),
          TextFormField(
            autocorrect: false,
            obscureText: true,
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecorations.authInputDecoration(
              hintText: '*****',
              labelText: 'Contraseña',
              prefixIcon: Icons.lock_outline,
            ),
            onChanged: (value) => loginForm.password = value,
            validator: (value) {
              return (value != null && value.length >= 6)
                  ? null
                  : 'La contraseña debe de ser de 6 caracteres';
            },
          ),
          const SizedBox(height: 30),
          CheckboxListTile(
              activeColor: '#004650'.toColor(),
              title: const Text('Recordar Contraseña'),
              value: _sliderEnabled,
              onChanged: (value) => setState(() {
                    _sliderEnabled = value ?? true;
                  })),
          MaterialButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              disabledColor: Colors.grey,
              elevation: 0,
              color: '#004650'.toColor(),
              child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 80, vertical: 15),
                  child: Text(
                    loginForm.isLoading ? 'Espere' : 'Iniciar sesión',
                    style: const TextStyle(color: Colors.white),
                  )),
              onPressed: loginForm.isLoading
                  ? null
                  : () async {
                      FocusScope.of(context).unfocus();

                      if (!loginForm.isValidForm()) return;

                      loginForm.isLoading = true;
                      UserRequestModel model = UserRequestModel(
                          email: loginForm.email, password: loginForm.password);
                      APIService.login(model).then((response) => {
                            if (response)
                              {Navigator.pushReplacementNamed(context, 'home')}
                            else
                              {
                                NotificationsService.showSnackbar(
                                    "Usuario o Contraseña Incorrectos"),
                                loginForm.isLoading = false,
                              }
                          });
                    })
        ],
      ),
    );
  }
}
