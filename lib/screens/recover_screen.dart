import 'package:bancapp/utils/extensions.dart';
import 'package:bancapp/widgets/widgets.dart';
import 'package:flutter/material.dart';

class RecoverScreen extends StatelessWidget {
  const RecoverScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double widthDevice = MediaQuery.of(context).size.width;
    double heightDevice = MediaQuery.of(context).size.height;

    return Scaffold(
      backgroundColor: '#09545f'.toColor(),
      body: Center(
        child: SizedBox(
            height: heightDevice - 310,
            width: widthDevice - 80,
            child: const CustomCard()),
      ),
    );
  }
}
