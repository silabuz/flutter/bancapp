import 'package:bancapp/services/shared_service.dart';
import 'package:bancapp/utils/extensions.dart';
import 'package:flutter/material.dart';

import '../widgets/widgets.dart';

const List<String> list = <String>[
  'Banco de Crédito del Perú',
  'Scotiabank',
  'BBVA',
  'MiBanco'
];
String dropdownValue = list.first;

class Perfilcreen extends StatefulWidget {
  const Perfilcreen({Key? key}) : super(key: key);

  @override
  State<Perfilcreen> createState() => _PerfilcreenState();
}

class _PerfilcreenState extends State<Perfilcreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 10,
          centerTitle: true,
          title: const Text('Perfil', textAlign: TextAlign.center),
          actions: const [
            Icon(Icons.notifications),
            SizedBox(
              width: 10,
            )
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              // Imagen
              Stack(children: [
                const SizedBox(
                  height: 250,
                  width: double.infinity,
                  child: Image(
                    image: AssetImage('assets/perfIlB.jpg'),
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Container(
                  height: 250,
                  width: double.infinity,
                  decoration:
                      const BoxDecoration(color: Color.fromRGBO(0, 0, 0, 0.2)),
                ),
                const Positioned(
                    right: 20,
                    top: 20,
                    child: Icon(
                      Icons.add_a_photo_rounded,
                      color: Colors.white,
                      size: 38,
                    )),
                Positioned(
                    left: 20,
                    bottom: 20,
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Icon(
                            Icons.account_circle,
                            color: Colors.white,
                            size: 30,
                          ),
                          const SizedBox(width: 15),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text('Nombres y Apellidos',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15)),
                              Text('Jorge Vicuña Valle',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 22))
                            ],
                          )
                        ])),
              ]),
              Container(
                margin: const EdgeInsets.all(10),
                padding:
                    const EdgeInsets.symmetric(horizontal: 5, vertical: 15),
                decoration: BoxDecoration(
                  border: Border.all(
                      color: const Color.fromRGBO(220, 220, 220, 0.8),
                      width: 0.8),
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Column(children: [
                  const CellPerfil(
                    title: "Celular",
                    subTitle: "999339593",
                    icon: Icons.phone_android,
                  ),

                  const CellPerfil(
                    title: "Correo",
                    subTitle: "jorge150896@hotmail.com",
                    icon: Icons.email,
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 20),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            child: Row(children: [
                              const Icon(Icons.arrow_drop_down),
                              const SizedBox(width: 30),
                              Expanded(
                                child: Container(
                                    height: 1,
                                    color: const Color.fromRGBO(
                                        220, 220, 220, 0.9)),
                              )
                            ]),
                          ),
                          const SizedBox(height: 10),
                          Container(
                            padding: const EdgeInsets.only(left: 5),
                            child: const Text('Datos Administrativos',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500)),
                          ),
                          const SizedBox(height: 10),
                        ]),
                  ),

                  const CellPerfil(
                      title: "Condicion",
                      subTitle: "Full time",
                      icon: Icons.watch_later_outlined),
                  // Description
                  CellPerfil(
                      title: "Banco Sueldo",
                      subTitle: "Banco de Crédito del Perú",
                      icon: Icons.account_balance,
                      textAction: "CAMBIAR BANCO",
                      onPressed: () =>
                          displayDialog(context, Item.changeButton)),

                  const CellPerfil(
                      title: "Condicion",
                      subTitle: "Full time",
                      icon: Icons.watch_later_outlined),
                  // Description
                  const CellPerfil(
                    title: "Banco Sueldo",
                    subTitle: "Banco de Crédito del Perú",
                    icon: Icons.account_balance,
                  ),

                  CellPerfil(
                      title: "Sistema de Pensión",
                      subTitle: "AFP Prima Mixta",
                      icon: Icons.business_center_rounded,
                      textAction: "AFILIACION AFP",
                      onPressed: () => displayDialog(context, Item.membership))
                ]),
              ),
              const SizedBox(height: 20),

              ButtonAction(
                title: 'Cambiar contraseña',
                onPressed: () => Navigator.pushNamed(context, "recover"),
              ),
              const SizedBox(height: 20),
              ButtonAction(
                title: 'Prueba Token',
                onPressed: () async => await SharedService.loginDetails()
                    .then((value) => print(value?.token)),
              ),
              const SizedBox(height: 20),
              ButtonAction(
                title: 'Cerrar Sesion',
                onPressed: () => SharedService.logout(context),
              ),
              const SizedBox(height: 40),
            ],
          ),
        ));
  }

  void displayDialog(BuildContext context, Item item) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          switch (item) {
            case Item.changeButton:
              return AlertDialog(
                elevation: 5,
                title: const Text('Cambiar Banco Sueldo'),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(10)),
                content: StatefulBuilder(
                    builder: (BuildContext context, StateSetter setState) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(height: 20),
                      const Text('Banco Actual',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                      const SizedBox(height: 10),
                      const Text('Banco de Crédito del Perú',
                          style: TextStyle(color: Colors.black, fontSize: 16)),
                      const SizedBox(height: 40),
                      const Text('Solicitar Cambio de Banco a',
                          style: TextStyle(color: Colors.grey, fontSize: 14)),
                      DropdownButton<String>(
                        isExpanded: true,
                        value: dropdownValue,
                        icon: const Icon(
                          Icons.keyboard_arrow_down_rounded,
                          color: Color.fromARGB(255, 4, 37, 21),
                        ),
                        elevation: 16,
                        underline: Container(
                          height: 1,
                          color: '#004650'.toColor(),
                        ),
                        onChanged: (String? value) {
                          // This is called when the user selects an item.
                          setState(() {
                            dropdownValue = value!;
                          });
                        },
                        items:
                            list.map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ],
                  );
                }),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('CANCELAR',
                        style:
                            TextStyle(color: Color(0xffcc3162), fontSize: 17)),
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('SOLICITAR',
                        style:
                            TextStyle(color: Color(0xffcc3162), fontSize: 17)),
                  ),
                ],
              );
            case Item.membership:
              return AlertDialog(
                elevation: 5,
                title: const Text('Confirmar'),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadiusDirectional.circular(10)),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 10),
                    const Text(
                        'Tener en cuenta que: \n\n- Los nuevos afiliados al Sistema Privado de Pensiones deberán ser dados de alta en AFP Integra - Ley 29903 y su reglamento.'),
                    const Text(
                        '- Solo se afiliará al personal que no haya aportado anteriormente a ninguna AFP.'),
                    RichText(
                      text: const TextSpan(
                        text:
                            'Verificar en el siguiente link si usted ya se encuentra afiliado ',
                        style: TextStyle(color: Colors.black87, fontSize: 16),
                        children: <TextSpan>[
                          TextSpan(
                              text:
                                  'https:reporte\ndeudas.sbs.gob.pe/ReporteSituacion\nPrevisional/Afil_Consulta.aspx',
                              style: TextStyle(
                                  color: Color(0xffcc3162), fontSize: 16)),
                        ],
                      ),
                    ),
                    const Text(
                        '- Todas sus aportaciones realizadas a la ONP se pierden cuando se traslada a la AFP.'),
                    const SizedBox(height: 10),
                  ],
                ),
                actions: [
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('CANCELAR',
                        style:
                            TextStyle(color: Color(0xffcc3162), fontSize: 17)),
                  ),
                  TextButton(
                    onPressed: () => Navigator.pop(context),
                    child: const Text('CONFIRMAR\nAFILIACIÓN',
                        style:
                            TextStyle(color: Color(0xffcc3162), fontSize: 17)),
                  )
                ],
              );
          }
        });
  }
}

enum Item { changeButton, membership }
