import 'package:flutter/material.dart';

class AsistenciaScreen extends StatelessWidget {
  const AsistenciaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: const Text('Asistencia'),
      ),
      body: const Center(
        child: Text('Asistencia'),
      ),
    );
  }
}
