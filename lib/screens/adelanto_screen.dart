import 'package:flutter/material.dart';

class AdelantoScreen extends StatelessWidget {
  const AdelantoScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        title: const Text('Adelanto'),
      ),
      body: const Center(
        child: Text('Adelanto'),
      ),
    );
  }
}
