import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bancapp/screens/screens.dart';

import 'package:bancapp/providers/ui_provider.dart';

import 'package:bancapp/widgets/widgets.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _HomePageBody(),
      bottomNavigationBar: const CustomNavigationBar(),
    );
  }
}

class _HomePageBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Obtener el selected menu opt
    final uiProvider = Provider.of<UiProvider>(context);

    // Cambiar para mostrar la pagina respectiva
    final currentIndex = uiProvider.selectedMenuOpt;

    switch (currentIndex) {
      case 0:
        return const Perfilcreen();

      case 1:
        return const AsistenciaScreen();

      case 2:
        return const AdelantoScreen();

      case 3:
        return const CreditoScreen();

      default:
        return const MenuScreen();
    }
  }
}
