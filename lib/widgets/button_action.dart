import 'package:flutter/material.dart';

class ButtonAction extends StatelessWidget {
  final String title;
  final GestureTapCallback onPressed;

  const ButtonAction({Key? key, required this.title, required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double widthDevice = MediaQuery.of(context).size.width;

    return SizedBox(
      width: double.infinity,
      child: Center(
          child: SizedBox(
        width: widthDevice - 50,
        child: ElevatedButton(
          onPressed: () {
            onPressed();
          },
          style: TextButton.styleFrom(
            backgroundColor: const Color(0xffcc3162),
            minimumSize: const Size.fromHeight(
                50), // fromHeight use double.infinity as width and 40 is the height
          ),
          child: Text(title,
              style: const TextStyle(color: Colors.white, fontSize: 18)),
        ),
      )),
    );
  }
}
