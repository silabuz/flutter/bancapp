import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:bancapp/providers/ui_provider.dart';
import 'package:bancapp/utils/extensions.dart';

class CustomNavigationBar extends StatelessWidget {
  const CustomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final uiProvider = Provider.of<UiProvider>(context);

    final currentIndex = uiProvider.selectedMenuOpt;

    return BottomNavigationBar(
      onTap: (int i) => uiProvider.selectedMenuOpt = i,
      currentIndex: currentIndex,
      selectedItemColor: '#004650'.toColor(), //item seleccionado
      type: BottomNavigationBarType.fixed, // permite poner mas de 3 elementos
      elevation: 20,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Perfil'),
        BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today), label: 'Asistencia'),
        BottomNavigationBarItem(
            icon: Icon(Icons.monetization_on_outlined), label: 'Adelanto'),
        BottomNavigationBarItem(
            icon: Icon(Icons.credit_card), label: 'Credito'),
        BottomNavigationBarItem(icon: Icon(Icons.menu), label: 'Menu'),
      ],
    );
  }
}
