import 'package:bancapp/utils/extensions.dart';
import 'package:flutter/material.dart';

class CustomCard extends StatefulWidget {
  const CustomCard({
    Key? key,
  }) : super(key: key);

  @override
  State<CustomCard> createState() => _CustomCardState();
}

class _CustomCardState extends State<CustomCard> {
  bool _isObscure = true;
  @override
  Widget build(BuildContext context) {
    return Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5),
          side: const BorderSide(
            width: 0.5,
            color: Colors.white,
          ),
        ),
        child: Column(
          children: [
            Container(
              color: '#00444e'.toColor(),
              height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.edit,
                    color: Colors.white,
                    size: 24,
                  ),
                  SizedBox(width: 10),
                  Text(
                    "Cambiar contraseña",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Column(
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  const Text(
                    "Por temas de seguridad, su nombre de usuario no debe ser igual a su contraseña",
                    style: TextStyle(color: Colors.black, fontSize: 15),
                  ),
                  const SizedBox(
                    height: 40,
                  ),
                  TextField(
                    obscureText: _isObscure,
                    decoration: InputDecoration(
                        labelText: 'Contraseña nueva',
                        // this button is used to toggle the password visibility
                        suffixIcon: IconButton(
                            icon: Icon(_isObscure
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                _isObscure = !_isObscure;
                              });
                            })),
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  TextField(
                    obscureText: _isObscure,
                    decoration: InputDecoration(
                        labelText: 'Repite la contraseña',
                        // this button is used to toggle the password visibility
                        suffixIcon: IconButton(
                            icon: Icon(_isObscure
                                ? Icons.visibility
                                : Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                _isObscure = !_isObscure;
                              });
                            })),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('CANCELAR',
                            style: TextStyle(
                                color: Color(0xffcc3162), fontSize: 17)),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: const Text('ACEPTAR',
                            style: TextStyle(
                                color: Color(0xffcc3162), fontSize: 17)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
