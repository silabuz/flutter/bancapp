import 'package:bancapp/utils/extensions.dart';
import 'package:flutter/material.dart';

class CellPerfil extends StatelessWidget {
  final String title;
  final String subTitle;
  final IconData icon;
  final String? textAction;
  final GestureTapCallback? onPressed;

  const CellPerfil({
    Key? key,
    required this.title,
    required this.subTitle,
    required this.icon,
    this.onPressed,
    this.textAction,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (onPressed != null) {
          onPressed!(); // Complains here
        }
      },
      child: Column(children: [
        ListTile(
          leading: Icon(icon, color: '#004650'.toColor(), size: 25),
          title: Text(
            title,
            style: const TextStyle(color: Colors.grey),
          ),
          subtitle: Text(
            subTitle,
            style: const TextStyle(color: Colors.black, fontSize: 18),
          ),
        ),
        if (textAction != null)
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 15),
            width: double.infinity,
            child: Align(
              alignment: Alignment.bottomRight,
              child: Text(
                textAction ?? "",
                textAlign: TextAlign.end,
                style: const TextStyle(
                    color: Color(0xffcc3162),
                    fontSize: 20,
                    fontWeight: FontWeight.w500),
              ),
            ),
          )
      ]),
    );
  }
}
